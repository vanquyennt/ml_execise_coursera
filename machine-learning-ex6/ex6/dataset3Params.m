function [C, sigma] = dataset3Params(X, y, Xval, yval)
%DATASET3PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = DATASET3PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%

value_list = [0.01, 0.03, 0.05, 0.1, 0.3, 0.5, 1, 3, 5, 10, 30, 50];
try_number = size(value_list, 2);

cost_min = 10^10;
final = [0, 0];
for index_C = 1:try_number
  for index_sigma = 1:try_number
    temp_C = value_list(1, index_C);
    temp_sigma = value_list(1, index_sigma);
    
    model = svmTrain(X, y, temp_C, @(x1, x2) gaussianKernel(x1, x2, temp_sigma));
    
    y_predict = svmPredict(model, Xval);
    cost = mean(double(y_predict ~= yval));
    
    if cost < cost_min
      cost_min = cost;
      final = [temp_C, temp_sigma]
    endif
  endfor
endfor

C = final(1, 1)
sigma = final(1, 2)


% =========================================================================

end
