function idx = findClosestCentroids(X, centroids)
%FINDCLOSESTCENTROIDS computes the centroid memberships for every example
%   idx = FINDCLOSESTCENTROIDS (X, centroids) returns the closest centroids
%   in idx for a dataset X where each row is a single example. idx = m x 1 
%   vector of centroid assignments (i.e. each entry in range [1..K])
%

% Set K
K = size(centroids, 1);

% You need to return the following variables correctly.
idx = zeros(size(X,1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Go over every example, find its closest centroid, and store
%               the index inside idx at the appropriate location.
%               Concretely, idx(i) should contain the index of the centroid
%               closest to example i. Hence, it should be a value in the 
%               range 1..K
%
% Note: You can use a for-loop over the examples to compute this.
%

for i = 1: size(X,1)
  current_x = X(i, :);
  
  cost = 10^10;
  final_index = 1;
  for j = 1: K
    current_cen = centroids(j, :);
    cost_temp = sum((current_x - current_cen).^2);
    
    if cost_temp < cost
      cost = cost_temp;
      final_index = j;
    endif
  endfor
  
  idx(i, 1) = final_index;
endfor

% =============================================================

end

