function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

corect_X = [ones(m, 1), X];

h_2 = sigmoid(corect_X*Theta1'); % The size of it is (m, hidden_layer_size)
h_2 = [ones(m, 1), h_2]; % add bias element 
h_3 = sigmoid(h_2*Theta2'); % the size of it is (m, num_labels)

y_final = zeros(m, num_labels);
for c = 1:num_labels
  temp_y = (y == c);
  y_final(:, c) = temp_y; 
endfor

Theta1_adapt = Theta1; Theta1_adapt(:, 1) = 0;
Theta2_adapt = Theta2; Theta2_adapt(:, 1) = 0;
cost_lambda = sum(sum(Theta1_adapt.^2)) + sum(sum(Theta2_adapt.^2));

J = 1/m * sum(sum(-y_final.*log(h_3) - (1-y_final).*log(1-h_3))) + lambda/(2*m)*cost_lambda;

% -------------------------------------------------------------

for t = 1:m
  x = X(t, :); x = [1, x];
  h_grad_2 = sigmoid(x*Theta1'); h_grad_2 = [1, h_grad_2];
  h_grad_3 = sigmoid(h_grad_2*Theta2');
  
  delta_3 = h_grad_3 - y_final(t, :); % size of it is (1, num_labels)
  z_2 = x*Theta1'; z_2 = [1, z_2];
  delta_2 = (delta_3*Theta2).*sigmoidGradient(z_2); % size of it is (1, hidden_layer_size+1)
  
  Theta2_grad = Theta2_grad + delta_3'*h_grad_2;
  temp = delta_2(1, 2:end);
  Theta1_grad = Theta1_grad + temp'*x;
  %temp = (delta_2'*x);
  %Theta1_grad = Theta1_grad + temp(2:end, :);
endfor

Theta1_grad = Theta1_grad/m + lambda/m*Theta1_adapt; 
Theta2_grad = Theta2_grad/m + lambda/m*Theta2_adapt;
% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
